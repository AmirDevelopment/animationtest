package com.amirgb.util;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.animation.BounceInterpolator;
import android.view.animation.DecelerateInterpolator;
import android.view.animation.Interpolator;

import java.lang.reflect.InvocationTargetException;

/**
 * Created by amirgb on 11/10/16.
 */

public class Util {

    public static class Animation {
        /**
         * Init the activity to show with circular reveal
         *
         * @param savedInstanceState
         * @param rootLayout
         * @param target
         */
        public static void initActivityCircularReveal(final Bundle savedInstanceState,
                                                      final View rootLayout, final View target) {
            if (savedInstanceState == null) {
                rootLayout.setVisibility(View.INVISIBLE);
                ViewTreeObserver viewTreeObserver = rootLayout.getViewTreeObserver();
                if (viewTreeObserver.isAlive()) {
                    viewTreeObserver.addOnGlobalLayoutListener(
                            new ViewTreeObserver.OnGlobalLayoutListener() {
                                @Override
                                public void onGlobalLayout() {
                                    circularReveal(rootLayout, target, 1000, 1, null);
                                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                                        rootLayout.getViewTreeObserver()
                                                .removeGlobalOnLayoutListener(this);
                                    } else {
                                        rootLayout.getViewTreeObserver()
                                                .removeOnGlobalLayoutListener(this);
                                    }
                                }
                            });
                }
            }
        }

        /**
         * Animation to show view with circular reveal
         *
         * @param rootLayout
         * @param target
         * @param runnable
         */
        public static void circularReveal(final View rootLayout,
                                          final View target,
                                          final int duration,
                                          float factor,
                                          final Runnable runnable) {
            int cx = target.getLeft();
            int cy = target.getTop();

            if (factor <= 0)
                factor = 1;

            float finalRadius = (float) Math.hypot(cx, cy) * factor;

            Animator anim = null;

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                anim = ViewAnimationUtils.createCircularReveal(rootLayout, cx, cy, 0, finalRadius);
                rootLayout.setVisibility(View.VISIBLE);
                initObjectAnimator(anim, duration, null, runnable);
            } else {
                rootLayout.setVisibility(View.VISIBLE);
                if (runnable != null)
                    runnable.run();
            }
        }

        /**
         * Animation to show view with circular reveal
         *
         * @param rootLayout
         * @param targetX
         * @param targetY
         * @param runnable
         */
        public static void circularReveal(final View rootLayout,
                                          final int targetX,
                                          final int targetY,
                                          final int duration,
                                          float factor,
                                          final Runnable runnable) {
            int cx = targetX;
            int cy = targetY;

            if (factor <= 0)
                factor = 1;

            float finalRadius = (float) Math.hypot(cx, cy) * factor;

            Animator anim = null;

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                anim = ViewAnimationUtils.createCircularReveal(rootLayout, cx, cy, 0, finalRadius);
                rootLayout.setVisibility(View.VISIBLE);
                initObjectAnimator(anim, duration, null, runnable);
            } else {
                rootLayout.setVisibility(View.VISIBLE);
                if (runnable != null)
                    runnable.run();
            }
        }

        /**
         * Animation to hide view with circular reveal
         *
         * @param rootLayout
         * @param target
         * @param runnable
         */
        public static void circularUnreveal(final View rootLayout,
                                            final View target,
                                            final Runnable runnable) {
            int cx = target.getLeft();
            int cy = target.getTop();
            float finalRadius = (float) Math.hypot(cx, cy);
            Animator anim = null;

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                anim = ViewAnimationUtils.createCircularReveal(rootLayout, 0, 0, finalRadius, 0);
                rootLayout.setVisibility(View.VISIBLE);
                initObjectAnimator(anim, 1000, null, new Runnable() {
                    @Override
                    public void run() {
                        rootLayout.setVisibility(View.INVISIBLE);
                        if (runnable != null)
                            runnable.run();
                    }
                });
            } else {
                if (runnable != null)
                    runnable.run();
            }
        }

        /**
         * Animation for activity entrance as drawer
         *
         * @param activity
         */
        public static void entranceDrawerActivity(Activity activity) {
            activity.overridePendingTransition(R.anim.pull_in_from_left, R.anim.hold);
        }

        /**
         * Animation for exit entrance as drawer
         *
         * @param activity
         */
        public static void exitDrawerActivity(Activity activity) {
            activity.overridePendingTransition(R.anim.hold, R.anim.push_out_to_left);
        }

        /**
         * Animation for activity entrance from right and pull parent to left
         *
         * @param activity
         */
        public static void entranceSlideRightInLeftOut(Activity activity) {
            activity.overridePendingTransition(R.anim.right_in, R.anim.left_out);
        }

        /**
         * Animation for activity entrance from right and pull parent to left
         *
         * @param activity
         */
        public static void exitSlideLeftInRightOut(Activity activity) {
            activity.overridePendingTransition(R.anim.left_in, R.anim.right_out);
        }

        /**
         * Animation for view - move to Y position and bounce
         *
         * @param view
         * @param y
         * @param duration
         * @param runnable
         */
        public static void fallAndBounce(final View view, final int y, final int duration,
                                         final Runnable runnable) {
            moveVertically(view, y, duration, new BounceInterpolator(), runnable);
        }

        /**
         * Animation for view - move to Y position
         *
         * @param view
         * @param y
         * @param duration
         * @param interpolator
         * @param runnable
         */
        public static void moveVertically(final View view, final int y, final int duration,
                                          final Interpolator interpolator,
                                          final Runnable runnable) {
            Animator moveAnim = ObjectAnimator.ofFloat(view, "Y", y);
            initObjectAnimator(moveAnim, duration, interpolator, runnable);
        }

        /**
         * Animation for view - move to X position
         *
         * @param view
         * @param x
         * @param duration
         * @param interpolator
         * @param runnable
         */
        public static void moveHorizontally(final View view, final int x, final int duration,
                                            final Interpolator interpolator,
                                            final Runnable runnable) {
            Animator moveAnim = ObjectAnimator.ofFloat(view, "X", x);
            initObjectAnimator(moveAnim, duration, interpolator, runnable);
        }

        /**
         * move through x and y
         *
         * @param view
         * @param x
         * @param y
         * @param duration
         * @param runnable
         */

        public static void moveToPoint(final View view, final int x, final int y, final int duration,
                                       final Runnable runnable) {
            Animator moveAnimX = ObjectAnimator.ofFloat(view, "X", x);
            moveAnimX = initObjectAnimator(moveAnimX, duration, null, null);
            Animator moveAnimY = ObjectAnimator.ofFloat(view, "Y", y);
            moveAnimY = initObjectAnimator(moveAnimY, duration, null, null);
            AnimatorSet animatorSet = new AnimatorSet();
            initSetAnimator(animatorSet, duration, runnable, moveAnimX, moveAnimY);

        }

        /**
         * Add rotation animation
         *
         * @param view
         * @param rotation
         * @param duration
         * @param interpolator
         * @param runnable
         */
        public static void rotate(final View view, final int rotation, final int duration,
                                  final Interpolator interpolator,
                                  final Runnable runnable) {
            Animator moveAnim = ObjectAnimator.ofFloat(view, "rotation", rotation);
            initObjectAnimator(moveAnim, duration, interpolator, runnable);
        }

        /**
         * Resize the width of view
         *
         * @param view
         * @param from
         * @param to
         * @param duration
         * @param interpolator
         * @param runnable
         */
        public static void scaleX(final View view, final int from, int to, final int duration,
                                  final Interpolator interpolator,
                                  final Runnable runnable) {
            Animator moveAnim = ObjectAnimator.ofFloat(view, "scaleX", from, to);
            initObjectAnimator(moveAnim, duration, interpolator, runnable);
        }

        /**
         * Resize the height of view
         *
         * @param view
         * @param from
         * @param to
         * @param duration
         * @param interpolator
         * @param runnable
         */
        public static void scaleY(final View view, final int from, int to, final int duration,
                                  final Interpolator interpolator,
                                  final Runnable runnable) {
            Animator moveAnim = ObjectAnimator.ofFloat(view, "scaleY", from, to);
            initObjectAnimator(moveAnim, duration, interpolator, runnable);
        }

        /**
         * Resize height and width of view
         *
         * @param view
         * @param fromX
         * @param toX
         * @param fromY
         * @param toY
         * @param duration
         * @param interpolator
         * @param runnable
         */
        public static void scaleXY(final View view, final int fromX, final int toX,
                                   final int fromY, final int toY, final int duration,
                                   final Interpolator interpolator,
                                   final Runnable runnable) {

            Animator moveAnimX = ObjectAnimator.ofFloat(view, "scaleX", fromX, toX);
            moveAnimX = initObjectAnimator(moveAnimX, duration, null, null);
            Animator moveAnimY = ObjectAnimator.ofFloat(view, "scaleY", fromY, toY);
            moveAnimY = initObjectAnimator(moveAnimY, duration, null, null);
            AnimatorSet animatorSet = new AnimatorSet();
            initSetAnimator(animatorSet, duration, runnable, moveAnimX, moveAnimY);

        }

        /**
         * Change the alpha property of view
         *
         * @param view
         * @param from
         * @param to
         * @param duration
         * @param interpolator
         * @param runnable
         */
        public static void vanish(final View view, final int from, int to, final int duration,
                                  final Interpolator interpolator,
                                  final Runnable runnable) {
            Animator moveAnim = ObjectAnimator.ofFloat(view, "alpha", from, to);
            initObjectAnimator(moveAnim, duration, interpolator, runnable);
        }

        /**
         * Init object animator
         *
         * @param moveAnim
         * @param duration
         * @param interpolator
         * @param runnable
         */
        private static Animator initObjectAnimator(final Animator moveAnim, final int duration,
                                                   final Interpolator interpolator,
                                                   final Runnable runnable) {
            moveAnim.setDuration(duration);
            if (interpolator != null)
                moveAnim.setInterpolator(interpolator);
            else
                moveAnim.setInterpolator(new DecelerateInterpolator());
            moveAnim.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {
                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    if (runnable != null)
                        runnable.run();
                }

                @Override
                public void onAnimationCancel(Animator animation) {
                }

                @Override
                public void onAnimationRepeat(Animator animation) {
                }
            });
            moveAnim.start();
            return moveAnim;
        }

        private static void initSetAnimator(final AnimatorSet animatorSet, final int duration, final Runnable runnable, final Animator... animators) {
            animatorSet.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {

                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    if (runnable != null)
                        runnable.run();
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });
            animatorSet.setDuration(duration);
            animatorSet.playTogether(animators);
            animatorSet.start();
        }


    }

    public static class Screen {
        private static final String DENSITY_LDPI = "ldpi";
        private static final String DENSITY_MDPI = "mdpi";
        private static final String DENSITY_HDPI = "hdpi";
        private static final String DENSITY_XHDPI = "xhdpi";
        private static final String DENSITY_XXHDPI = "xxhdpi";
        private static final String DENSITY_XXXHDPI = "xxxhdpi";

        /**
         * Gets navigation bar size
         *
         * @param context
         * @return
         */
        public static Point getNavigationBarSize(Context context) {
            Point appUsableSize = getAppUsableScreenSize(context);
            Point realScreenSize = getRealScreenSize(context);

            if (appUsableSize.x < realScreenSize.x) {
                return new Point(realScreenSize.x - appUsableSize.x, appUsableSize.y);
            }

            if (appUsableSize.y < realScreenSize.y) {
                return new Point(appUsableSize.x, realScreenSize.y - appUsableSize.y);
            }

            return new Point();
        }

        /**
         * Get area of screen that that user can interact
         *
         * @param context
         * @return
         */
        public static Point getAppUsableScreenSize(Context context) {
            WindowManager windowManager = (WindowManager)
                    context.getSystemService(Context.WINDOW_SERVICE);
            Display display = windowManager.getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
            return size;
        }

        /**
         * get real size of screen
         *
         * @param context
         * @return
         */
        public static Point getRealScreenSize(Context context) {
            WindowManager windowManager = (WindowManager)
                    context.getSystemService(Context.WINDOW_SERVICE);
            Display display = windowManager.getDefaultDisplay();
            Point size = new Point();

            if (Build.VERSION.SDK_INT >= 17) {
                display.getRealSize(size);
            } else if (Build.VERSION.SDK_INT >= 14) {
                try {
                    size.x = (Integer) Display.class.getMethod("getRawWidth").invoke(display);
                    size.y = (Integer) Display.class.getMethod("getRawHeight").invoke(display);
                } catch (IllegalAccessException e) {
                } catch (InvocationTargetException e) {
                } catch (NoSuchMethodException e) {
                }
            }
            return size;
        }

        /**
         * Return enum that represents the name of the density of the device
         *
         * @param activity
         * @return
         */
        public static String getDensityValueToString(Activity activity) {
            float density = getDensity(activity);
            if (density == 0.75f) {
                return DENSITY_LDPI;
            } else if (density == 1.0f) {
                return DENSITY_MDPI;
            } else if (density == 1.5f) {
                return DENSITY_HDPI;
            } else if (density == 2.0f) {
                return DENSITY_XHDPI;
            } else if (density == 3.0f) {
                return DENSITY_XXHDPI;
            } else if (density == 4.0f) {
                return DENSITY_XXXHDPI;
            }
            return "";
        }

        /**
         * return value of density
         *
         * @param activity
         * @return
         */
        public static float getDensity(Context activity) {
            return activity.getResources().getDisplayMetrics().density;
        }

        /**
         * transform dp in to pixels for programatically assign dimensions
         *
         * @param context
         * @param dp
         * @return
         */
        public static int dpToPx(Context context, float dp) {
            float scale = getDensity(context);
            return (int) ((dp * scale) + 0.5f);
        }
    }
}
