package com.amirgb.animation;

import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.amirgb.util.Util;

public class TransitionActivity extends AppCompatActivity {
    private int originaly = 0;
    private boolean expanded = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_childactivity);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        final FloatingActionButton fab1 = (FloatingActionButton) findViewById(R.id.fab1);
        final FloatingActionButton fab2 = (FloatingActionButton) findViewById(R.id.fab2);
        final FloatingActionButton fab3 = (FloatingActionButton) findViewById(R.id.fab3);
        final FloatingActionButton fab4 = (FloatingActionButton) findViewById(R.id.fab4);

        fab1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                originaly = fab1.getTop();

                Util.Animation.rotate(fab1, 90, 100, null, new Runnable() {
                    @Override
                    public void run() {
                        expanded = !expanded;
                        if (expanded) {

                            final int margin = (int) (Util.Screen.getDensity(TransitionActivity.this) * 16);
                            final int miniFabSize = (int) (Util.Screen.getDensity(TransitionActivity.this) * 40);
                            Util.Animation.vanish(fab2, 0, 1, 50, null, null);
                            Util.Animation.vanish(fab3, 0, 1, 50, null, null);
                            Util.Animation.vanish(fab4, 0, 1, 50, null, null);
                            fab2.setVisibility(View.INVISIBLE);
                            fab3.setVisibility(View.INVISIBLE);
                            fab4.setVisibility(View.INVISIBLE);

                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                                int fab2Translation = miniFabSize + margin;
                                fab2.setTranslationY(-(fab2Translation));
                                int fab3Translation = fab2Translation + miniFabSize
                                        + (margin / 2);
                                fab3.setTranslationY(-(fab3Translation));
                                int fab4Translation = fab3Translation + miniFabSize
                                        + (margin / 2);
                                fab4.setTranslationY(-(fab4Translation));
                                fab2.setVisibility(View.VISIBLE);
                                Util.Animation.scaleXY(fab2, 0, 1, 0, 1, 100, null, new Runnable() {
                                    @Override
                                    public void run() {
                                        fab3.setVisibility(View.VISIBLE);
                                        Util.Animation.scaleXY(fab3, 0, 1, 0, 1, 100, null, new Runnable() {
                                            @Override
                                            public void run() {
                                                fab4.setVisibility(View.VISIBLE);
                                                Util.Animation.scaleXY(fab4, 0, 1, 0, 1, 100, null, null);
                                            }
                                        });
                                    }
                                });

                            } else {

                                int fab2Translation = miniFabSize + margin + (margin / 2);
                                fab2.setTranslationY(-(fab2Translation));
                                int fab3Translation = fab2Translation + miniFabSize + (margin / 2);
                                fab3.setTranslationY(-(fab3Translation));
                                int fab4Translation = fab3Translation + miniFabSize + (margin / 2);
                                fab4.setTranslationY(-(fab4Translation));
                                fab2.setVisibility(View.VISIBLE);
                                Util.Animation.scaleXY(fab2, 0, 1, 0, 1, 100, null, new Runnable() {
                                    @Override
                                    public void run() {
                                        fab3.setVisibility(View.VISIBLE);
                                        Util.Animation.scaleXY(fab3, 0, 1, 0, 1, 100, null, new Runnable() {
                                            @Override
                                            public void run() {
                                                fab4.setVisibility(View.VISIBLE);
                                                Util.Animation.scaleXY(fab4, 0, 1, 0, 1, 100, null, null);
                                            }
                                        });
                                    }
                                });
                            }
                        } else {
                            Util.Animation.rotate(fab1, 0, 100, null, null);
//                            Util.Animation.moveVertically(fab2, originaly, 100, null, null);
//                            Util.Animation.moveVertically(fab3, originaly, 100, null, null);
//                            Util.Animation.moveVertically(fab4, originaly, 100, null, null);
                            Util.Animation.vanish(fab2, 1, 0, 100, null, null);
                            Util.Animation.vanish(fab3, 1, 0, 100, null, null);
                            Util.Animation.vanish(fab4, 1, 0, 100, null, null);
                        }
                    }
                });


            }
        });
    }

    @Override
    public void onBackPressed() {
        finish();
        overridePendingTransition(R.anim.left_in, R.anim.right_out);
    }

    // to handle activity transitions for Up navigation add it to the onOptionsItemSelected
    // as below
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        // This refers to the Up navigation button in the action bar
        if (id == android.R.id.home) {
            finish();
            Util.Animation.exitSlideLeftInRightOut(this);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
