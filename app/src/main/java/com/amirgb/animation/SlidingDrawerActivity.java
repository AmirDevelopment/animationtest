package com.amirgb.animation;

/**
 * Created by amirgb on 11/10/16.
 */

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.amirgb.util.Util;


public class SlidingDrawerActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Util.Animation.entranceDrawerActivity(this);
        setContentView(R.layout.activity_sliding_drawer);
    }

    @Override
    protected void onPause() {
        Util.Animation.exitDrawerActivity(this);
        super.onPause();
    }
}