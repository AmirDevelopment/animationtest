package com.amirgb.animation;

import android.animation.Animator;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewAnimationUtils;

import com.amirgb.util.Util;

public class RevealActivity extends AppCompatActivity {
    private View rootLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reveal);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        final FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

        rootLayout = findViewById(R.id.rootLayout);

        // init root layout
        Util.Animation.initActivityCircularReveal(savedInstanceState, rootLayout, fab);

    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

                Util.Animation.circularUnreveal(rootLayout, fab, new Runnable() {
                    @Override
                    public void run() {
                        finish();
                    }
                });
                return false;
        }
        return super.onOptionsItemSelected(item);
    }


    public void circularRevealAnimation(View view, final Runnable runnable) {
        // previously invisible view
        View myView = findViewById(R.id.rootLayout);

        int cx = view.getLeft();
        int cy = view.getTop();

        float finalRadius = (float) Math.hypot(cx, cy);

        Animator anim =
                null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            anim = ViewAnimationUtils.createCircularReveal(myView, cx, cy, 0, finalRadius);
            myView.setVisibility(View.VISIBLE);
            anim.setDuration(1000);
            anim.start();

        } else {
            runnable.run();
        }


        anim.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {

            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }


    public void inverseCircularRevealAnimation(View view, final Runnable runnable) {
        // previously invisible view
        View myView = findViewById(R.id.rootLayout);

        int cx = view.getLeft();
        int cy = view.getTop();

        float finalRadius = (float) Math.hypot(cx, cy);

        Animator anim =
                null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            anim = ViewAnimationUtils.createCircularReveal(myView, 0, 0, finalRadius, 0);
            myView.setVisibility(View.VISIBLE);
            anim.setDuration(1000);
            anim.start();
        } else {
            runnable.run();
        }


        anim.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                rootLayout.setVisibility(View.INVISIBLE);
                runnable.run();

            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

}
