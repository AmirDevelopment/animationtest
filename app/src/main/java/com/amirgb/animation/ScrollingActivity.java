package com.amirgb.animation;

import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import com.amirgb.util.Util;

public class ScrollingActivity extends AppCompatActivity {
    private FloatingActionButton fab;
    private View content;
    private View reveal;
    private AppBarLayout appBarLayout;

    private CollapsingToolbarLayout toolbarLayout;
    private int offset;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrolling);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        toolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);
        appBarLayout = (AppBarLayout) findViewById(R.id.app_bar);
        appBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                offset = verticalOffset;
            }
        });
        content = findViewById(R.id.content);
        reveal = findViewById(R.id.viewReveal);
        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Util.Animation.moveToPoint(fab, content.getMeasuredWidth() / 2,
                        appBarLayout.getBottom() + 100, 1000, new Runnable() {
                            @Override
                            public void run() {
                                fab.setVisibility(View.INVISIBLE);
                                Util.Animation.circularReveal(reveal, content.getMeasuredWidth() / 2,
                                        appBarLayout.getTop() + fab.getMeasuredHeight(), 3500, 10,
                                        new Runnable() {
                                            @Override
                                            public void run() {
                                            }
                                        });
                            }
                        });
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        // This refers to the Up navigation button in the action bar
        if (id == android.R.id.home) {
            finish();
            Util.Animation.exitSlideLeftInRightOut(this);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
