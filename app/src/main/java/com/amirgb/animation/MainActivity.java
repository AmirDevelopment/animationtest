package com.amirgb.animation;

import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.amirgb.util.Util;


public class MainActivity extends AppCompatActivity {
    int originalY;
    private Button buttonAnimateTransition;
    private Button buttonDrawer;
    private Button buttonFabToReveal;
    private View viewContent;
    private View viewReveal;
    private FloatingActionButton fabReveal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        buttonAnimateTransition = (Button) findViewById(R.id.button);
        buttonDrawer = (Button) findViewById(R.id.button2);
        viewReveal = findViewById(R.id.viewReveal);
        buttonFabToReveal = (Button) findViewById(R.id.button3);
        final Point mdispSize = Util.Screen.getAppUsableScreenSize(this);
        final int maxX = mdispSize.x;
        final int maxY = mdispSize.y;

        buttonDrawer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, SlidingDrawerActivity.class);
                startActivity(intent);
                Util.Animation.entranceDrawerActivity(MainActivity.this);
            }
        });

        buttonAnimateTransition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(MainActivity.this, mdispSize.x + " , " + mdispSize.y, Toast.LENGTH_LONG).show();
                viewContent = findViewById(R.id.activity_main);
                animate(buttonAnimateTransition);
            }
        });

        fabReveal = (FloatingActionButton) findViewById(R.id.fab);
        fabReveal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, RevealActivity.class);
                startActivity(intent);
                overridePendingTransition(0, 0);
            }
        });

        buttonFabToReveal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ScrollingActivity.class);
                startActivity(intent);
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        animationToOriginalPosition();
        viewReveal.setVisibility(View.INVISIBLE);
    }

    public void animationToOriginalPosition() {
        if (buttonAnimateTransition != null && viewContent != null) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Util.Animation.moveVertically(buttonAnimateTransition, originalY, 100, null, null);
                }
            });
        }
    }


    public void animate(View view) {
        originalY = buttonAnimateTransition.getTop();
        Util.Animation.moveVertically(buttonAnimateTransition, viewContent.getMeasuredHeight() - view.getMeasuredHeight(), 600, null, new Runnable() {
            @Override
            public void run() {
                Intent i = new Intent(MainActivity.this, TransitionActivity.class);
                startActivity(i);
                Util.Animation.entranceSlideRightInLeftOut(MainActivity.this);
            }
        });
    }

}
